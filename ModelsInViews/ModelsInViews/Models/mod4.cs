﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsInViews.Models
{
    public class mod4
    {
        public DateTime Today { get; set; }
        public DateTime Tomorrow { get; set; }
        public DateTime NextYear { get; set; }
    }
}
