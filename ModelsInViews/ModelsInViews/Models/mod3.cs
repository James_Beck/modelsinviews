﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsInViews.Models
{
    public class mod3
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Crime { get; set; }
        public string Sentence { get; set; }
    }
}
