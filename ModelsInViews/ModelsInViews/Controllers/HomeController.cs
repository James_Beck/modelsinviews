﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ModelsInViews.Models;

namespace ModelsInViews.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            mod1 mod = new mod1()
            {
                FirstName = "James",
                LastName = "Beck",
                Address = "Here",
                AreaCode = "14555",
                City = "Tauranga",
            };

            return View(mod);
        }

        public IActionResult Page1()
        {
            mod2 mod = new mod2()
            {
                First = 1,
                Second = 2,
                Third = 3,
                Fourth = 4,
                Fifth = 5,
            };

            return View(mod);
        }

        public IActionResult Page2()
        {
            mod3 mod = new mod3()
            {
                FirstName = "Pablo",
                LastName = "Escobar",
                Crime = "Drug Trafficking and Murder",
                Sentence = "A long time",
            };

            return View(mod);
        }

        public IActionResult Page3()
        {
            mod4 mod = new mod4()
            {
                Today = DateTime.Today,
                Tomorrow = DateTime.Today.AddDays(1),
                NextYear = DateTime.Today.AddYears(1),
            };

            return View(mod);
        }

        public IActionResult Page4()
        {
            mod5 mod = new mod5()
            {
                Truth = true,
                Lie = false,
            };

            return View(mod);
        }
    }
}
